int positive_test() {
    int x = 0;
    x += 5;//5
    x -= 2;//3
    x *= 9;//27 0x1b
    x /= 2;//13 0xd

    x %= 7;//6
    x |= 8;//14 0xe
    x ^= 3;//13 0xd

    x += 2 << 3;//13 + 16 = 29 0xd
    x -= 4 >> 1;//29 - 2 = 27 0x1b
    x &= 7;//3
    x <<= 5;//96 0x60
    x >>= 5;//3
    return x;
}

int main(){
    return positive_test();
}

int f(int n) {
    switch (n) {
        case 1:
        case 2:
            return 1;
        case 3:
            return 3;
        case 4:
            break;
        default:
            return 5;
    }
    return 4;
}

int main() {
    int a;
    // 1 + 1 +  3 + 4 + 5 = 14
    a = f(1) + f(2) + f(3) + f(4) + f(5);
    return a;// 14, 0xe
}

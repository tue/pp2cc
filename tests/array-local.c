int main(){
    int n = 4;
    int a[] = {5, 4, 3, n};
    // should return 5 + 4 + 4 = 13 = 0xd
    return *a + a[1] + a[3];
}

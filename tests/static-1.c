// Files: static-1.c static-2.c
// pp2cc should be able to compile two files with the same function defined,
// but using static storage
static int f() {
    return 1;
}
int main() {
    return f();
}
